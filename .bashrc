#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '
EDITOR=/usr/bin/vim
# Aliases
alias yaourt="echo Use pacaur instead! pacaur " # "pacaur " at the end allows bash to display the provided arguments as a code example
# xkbindkeys is used for binding keys.... in x
xbindkeys
