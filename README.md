# arch dotfiles

Requirements:

    i3-gaps [wm]

    i3bar [panel]

    i3blocks [panel]
    
    compton [compositor]
    
    termite [te]

programs for i3blocks (elements)

    amixer (volume mananger)
    
    spotify (not required, compatable)
